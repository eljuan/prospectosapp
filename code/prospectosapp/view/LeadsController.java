package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.MainApp;
import code.prospectosapp.model.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import java.sql.ResultSet;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;

public class LeadsController {

    private ObservableList<Lead> leadData = FXCollections.observableArrayList();
    
    private MainApp mainApp;

    @FXML
    private TableView<Lead> leadsTable;
    @FXML
    private TableColumn<Lead, String> nameColumn;
    @FXML
    private TableColumn<Lead, String> statusColumn;
    @FXML
    private TableColumn<Lead, String> userAssignedColumn;    
    
    @FXML
    private void initialize() {
		leadData.clear();
		try {
			Lead[] leadsSet = ApiAccess.loadLeads();
			int i = 0;
			while(i < leadsSet.length) {
				System.out.println("INSIDE LOOP");
				leadData.add(leadsSet[i]);
				i++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	public void handleAddLeadButtonAction(ActionEvent event) throws Exception {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/AddLeadDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        AddLeadDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleChangeAssignedUserButtonAction(ActionEvent event) throws Exception {
		IntegerProperty selectedLead ;
		try {
	    	try {
		        selectedLead = leadsTable.getSelectionModel().getSelectedItem().getId();
		    	MainApp.selectedLeadAdmin = selectedLead.getValue();
	    	} catch(NullPointerException e) {
	    		return;
	    	}
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/ChangeAssignedUser.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        ChangeAssignedUserDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleRemoveLeadButtonAction(ActionEvent event) throws Exception {
		IntegerProperty selectedLead ;
		try {
	    	try {
		        selectedLead = leadsTable.getSelectionModel().getSelectedItem().getId();
		    	System.out.println(selectedLead.getValue());
	    	} catch(NullPointerException e) {
	    		return;
	    	}
	    	// IF NOT USER SELECTED DONT DO ANYTHING.
	    	//MainApp.selectedUserAdmin = selectedUsername.getValue();
	    	MainApp.selectedLeadAdmin = selectedLead.getValue();
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/RemoveLead.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Remove Lead");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        RemoveLeadDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleMainMenuButtonAction(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AdminMenu.fxml"));
			AnchorPane usersWindow = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			MainApp.rootLayout.setCenter(usersWindow);
			//rootLayout.getChildren().add(login);
			AdminMenuController controller = loader.getController();
			controller.setMainApp(this.mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        System.out.println(leadsTable);
		leadsTable.setItems(this.getLeads());
		this.mainApp.setAdminLeads(this.getLeads());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
        statusColumn.setCellValueFactory(cellData -> cellData.getValue().getStatusString());
        userAssignedColumn.setCellValueFactory(cellData -> cellData.getValue().getAssignedString(this.mainApp.getUsers()));
    }    
    
    public ObservableList<Lead> getLeads() {
        return leadData;
    }
}
