package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.collections.*;
import code.prospectosapp.ApiAccess;

public class AddUserDialogController {

    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;
    @FXML
    private CheckBox adminField;



    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    private MainApp mainApp;
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     * 
     * @param person
     */

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * @throws Exception 
     */
    @FXML
    private void handleOk() throws Exception {
        if (isInputValid()) {
            String adminString = (adminField.isSelected()) ? "1" : "0";
            Boolean result = ApiAccess.AddUser(usernameField.getText(), passwordField.getText(), adminString);
            Boolean isAdmin = (adminString == "1") ? true : false;
            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (usernameField.getText() == null || usernameField.getText().length() == 0) {
            errorMessage += "Usuario no Valido!\n"; 
        }
        if (passwordField.getText() == null || passwordField.getText().length() < 6) {
            errorMessage += "Clave no valida, minimo 7 caracteres!\n"; 
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
			Alert a = new Alert(AlertType.INFORMATION);
	        a.setTitle("ProspectosApp");
	        a.setHeaderText("Error");
	        a.setContentText(errorMessage);
			a.showAndWait();
            return false;
        }
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    } 
    
}
