package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.model.Lead;
public class ShowLeadDialogController {

	public class KeyValuePair {
		   private final String key;
		   private final String value;
		   public KeyValuePair(String key, String value) {
		   this.key = key;
		   this.value = value;
		   }

		  public String getKey()   {    return key;    }

		  public String toString() {    return value;  }
		}
	
    @FXML
    private Label nameLabel;
    
    @FXML
    private Label companyLabel;
    
    @FXML
    private Label phoneLabel;
    
    @FXML
    private Label emailLabel;
    
    @FXML
    private Label addressLabel;
    
    @FXML
    private ChoiceBox<KeyValuePair> usersSelect = new ChoiceBox<KeyValuePair>();


    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    private MainApp mainApp;
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
   	
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     * 
     * @param person
     */

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * @throws Exception 
     */
    @FXML
    private void handleOk() throws Exception {
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

   public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    	ObservableList<Lead> leads = this.mainApp.getAdminLeads();
    	leads.forEach((lead) -> { 
    		if (lead.getId().get() == MainApp.selectedLeadUser) {
        	    StringProperty name = lead.getName();
        	    StringProperty address = lead.getAddress();
        	    StringProperty phone = lead.getPhone();
        	    StringProperty company = lead.getCompany();
        	    StringProperty email = lead.getEmail();
        	    
        	    nameLabel.setText(name.get());
        	    companyLabel.setText(company.get());
        	    phoneLabel.setText(phone.get());
        	    emailLabel.setText(email.get());
        	    addressLabel.setText(address.get());
    		}   	    ;
    	});
    } 
    
}
