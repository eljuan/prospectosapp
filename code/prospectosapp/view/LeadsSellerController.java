package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.MainApp;
import code.prospectosapp.model.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import java.sql.ResultSet;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;

public class LeadsSellerController {

    private ObservableList<Lead> leadData = FXCollections.observableArrayList();
    
    private MainApp mainApp;

    @FXML
    private TableView<Lead> leadsTable;
    @FXML
    private TableColumn<Lead, String> nameColumn;
    @FXML
    private TableColumn<Lead, String> statusColumn;
    @FXML
    private TableColumn<Lead, String> userAssignedColumn;    
    
    @FXML
    private void initialize() {
		leadData.clear();
		try {
			Lead[] leadsSet = ApiAccess.loadLeadsSeller(MainApp.loggedUserSeller);
			int i = 0;
			while(i < leadsSet.length) {
				leadData.add(leadsSet[i]);
				i++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	public void handleShowLeadButtonAction(ActionEvent event) throws Exception {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/ShowLeadDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	    	try {
	    		MainApp.selectedLeadUser = leadsTable.getSelectionModel().getSelectedItem().getId().get();
	    	} catch(NullPointerException e) {
	    		return;
	    	}	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        ShowLeadDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleChangeStatusButtonAction(ActionEvent event) throws Exception {
		IntegerProperty selectedLead ;
		try {
	    	try {
	    		MainApp.selectedLeadUser = leadsTable.getSelectionModel().getSelectedItem().getId().get();
	    	} catch(NullPointerException e) {
	    		return;
	    	}	
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/ChangeStatusLead.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        ChangeStatusLeadDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleExitButtonAction(ActionEvent event) throws Exception {
		System.exit(0);
	}
	
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;;
		leadsTable.setItems(this.getLeads());
		this.mainApp.setAdminLeads(this.getLeads());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
        statusColumn.setCellValueFactory(cellData -> cellData.getValue().getStatusString());
    }    
    
    public ObservableList<Lead> getLeads() {
        return leadData;
    }
}
