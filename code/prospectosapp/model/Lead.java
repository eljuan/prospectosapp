package code.prospectosapp.model;

import java.sql.ResultSet;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.MainApp;
import code.prospectosapp.view.UsersController;
import code.prospectosapp.view.ChangeAssignedUserDialogController.KeyValuePair;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Lead {

    private final StringProperty name;
    private final StringProperty company;
    private final StringProperty phone;
    private final StringProperty addresss;
    private final StringProperty email;	
    private final StringProperty status;
    private final StringProperty assigned_user;
    private StringProperty string_user;
	private final SimpleIntegerProperty id;	
    
    public Lead(Integer id, String name, String company, String phone, String address, String email, String status, String assigned_user) throws Exception
    {
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.company = new SimpleStringProperty(company);
		this.phone = new SimpleStringProperty(phone);
		this.addresss = new SimpleStringProperty(address);
		this.email = new SimpleStringProperty(email);
		this.status = new SimpleStringProperty(status);
		this.assigned_user = new SimpleStringProperty(assigned_user);		
		this.string_user = new SimpleStringProperty();	
	}
 
	public IntegerProperty getId() {
		return id;
	}
	
	public StringProperty getName() {
		return name;
	}

	public StringProperty getCompany() {
		return company;
	}

	public StringProperty getPhone() {
		return phone;
	}

	public StringProperty getAddress() {
		return addresss;
	}

	public StringProperty getEmail() {
		return email;
	}	
	
	public StringProperty getStatus() {
		return status;
	}	
	
	public StringProperty getStatusString() {
		String statusStr = "";
		String statusCode = String.valueOf(status.get());
		switch (statusCode) {
        case "0":  
        	statusStr = "No Contactado";
            break;
        case "1":
        	statusStr = "Contactado";
        	break;
        case "2":
        	statusStr = "No Interesado";
        	break;
        case "3":
        	statusStr = "Nuevo Cliente";
        	break;         	
		}
		return  new SimpleStringProperty(statusStr);
	}	

	public StringProperty getAssigned() {		
		return assigned_user;
	}
		
	public StringProperty getAssignedString(ObservableList<User> users) {
    	users.forEach((user) -> { 
    	    System.out.println("Stuff with "+user);
    	    StringProperty username = user.getUsername();
    	    Integer id_user = user.getId().get();
    	    if (id_user == Integer.valueOf(assigned_user.get())) {
    	    	this.string_user = username;
    	    }
    	});
    	return this.string_user; 
	}	
}



	