<?php
header('Content-type: application/json');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function conectarBD(){ 
        $server = "localhost";
        $usuario = "root";
        $pass = "practica";
        $BD = "prospectosapp";
        $conexion = mysqli_connect($server, $usuario, $pass, $BD); 
        if($conexion){ 
           return $conexion; 
        }else{ 
          return null; 
        } 
} 

function desconectarBD($conexion){
        $close = mysqli_close($conexion); 
        if($close){ 
           return $close;       
        }else{ 
           return null;
        }    
}

function loginUser($username, $password) {
	$conexion =  conectarBD();
	if ($conexion) {
		$sql = "SELECT * FROM users where username ='" . $username ."' and password= '".$password."'";
		$query = mysqli_query($conexion, $sql) or die(mysqli_error($conexion));
		$users = array();
		$row = $query->fetch_row();
		if ($row) {
			$final_result['result'] = true;
			$final_result['admin'] = $row[2];
			return $final_result;
		}
		$final_result['result'] = false;
		return $final_result;
	}
}

function insertUser($username, $password, $admin) {
	$conexion =  conectarBD();
	if ($conexion) {
		$sql = "INSERT INTO `users` (`username`, `password`, `admin`) VALUES ('".$username ."', '".$password."',".$admin.")";
		$query = mysqli_query($conexion, $sql) or die(mysqli_error($conexion));
		if ($query) {
			 $final_result['result'] = true;
			return $final_result;
		}
		return;
	}
}


function listUsers() {
	$conexion =  conectarBD();
	if ($conexion) {
		$sql = "SELECT * FROM users";
		$query = mysqli_query($conexion, $sql) or die(mysqli_error($conexion));
		$users = array();
		while ($row = $query->fetch_row()) {
			$object_user['username']=$row[0];
			$object_user['password']=$row[1];
			$object_user['admin']=$row[2];
			array_push($users, $object_user);
		}
		return $users;
	}
}

if (isset($_POST["username"]) and isset($_POST["password"])) {
	$resultLogin = loginUser( $_POST["username"], $_POST["password"]);
}
if ($resultLogin['result']) {
	switch($_POST["action"]) {
		case 'login':
			echo json_encode($resultLogin);
			return;
		case 'listUsers':
			if ($resultLogin) {
				$results= listUsers();
				if ($results) {
					echo json_encode($results);
					return;
				}
			}
		case 'insertUser':
			if (isset($_POST["i_username"]) and isset($_POST["i_password"]) and isset($_POST["i_admin"])) {
				$insert_result = insertUser($_POST["i_username"], $_POST["i_password"], $_POST["i_admin"]);
				echo json_encode($insert_result);
				return;
			}
		case 'changePassword':
		case 'deleteUser':	
		default:
			return;
	}
}
?>