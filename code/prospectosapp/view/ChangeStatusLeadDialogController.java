package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import code.prospectosapp.view.AddLeadDialogController.KeyValuePair;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.model.Lead;

public class ChangeStatusLeadDialogController {

	public class KeyValuePair {
		   private final String key;
		   private final String value;
		   public KeyValuePair(String key, String value) {
		   this.key = key;
		   this.value = value;
		   }

		  public String getKey()   {    return key;    }

		  public String toString() {    return value;  }
		}
	
    @FXML
    private ChoiceBox<KeyValuePair> leadStatusChoiceBox = new ChoiceBox<KeyValuePair>();

    @FXML
    private Label statusLeadLabel;
    
    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    private Integer selectedLead;
    private MainApp mainApp;
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     * 
     * @param person
     */

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * @throws Exception 
     */
    @FXML
    private void handleOk() throws Exception {
    	Integer a_leadstatus = Integer.valueOf(leadStatusChoiceBox.getValue().getKey());
    	if (a_leadstatus == null) {
    		return;
    	}
        Boolean result = ApiAccess.changeLeadStatus(MainApp.selectedLeadUser, a_leadstatus, MainApp.loggedUserSeller);
        okClicked = true;
        dialogStage.close();
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    	ObservableList<User> users = this.mainApp.getUsers();
    	ObservableList<Lead> leads = this.mainApp.getAdminLeads(); 	
    	leadStatusChoiceBox.getItems().add(new KeyValuePair(String.valueOf("0"), "No Contactado"));
    	leadStatusChoiceBox.getItems().add(new KeyValuePair(String.valueOf("1"), "Contactado"));
    	leadStatusChoiceBox.getItems().add(new KeyValuePair(String.valueOf("2"), "No Interesado"));
    	leadStatusChoiceBox.getItems().add(new KeyValuePair(String.valueOf("3"), "Nuevo Cliente"));
    	leads.forEach((leadToCheck) -> { ;
    		if (leadToCheck.getId().get() == MainApp.selectedLeadUser) {
    			statusLeadLabel.setText(leadToCheck.getStatusString().get());
    		}
    	});
    } 
  }
