package code.prospectosapp.model;

import java.sql.ResultSet;
import code.prospectosapp.ApiAccess;
import javafx.beans.property.StringProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class User {
	
	private final IntegerProperty id;
    private final StringProperty username;
    private final StringProperty password;
    private final BooleanProperty admin;
    
    public User(Integer id, String username, String password, Boolean admin) throws Exception
    {
    	this.id = new SimpleIntegerProperty(id);
		this.username = new SimpleStringProperty(username);
		this.password = new SimpleStringProperty(password);
		this.admin = new SimpleBooleanProperty(admin);
	}

    static public User doLogin(String username, String password) throws Exception
    {
    	final User userExists = ApiAccess.loginUser(username, password);
    	if (userExists != null) {
            return userExists;
    	} else  {
    		throw new Exception("invalid username / password");
    	}
	}
    
	public StringProperty getUsername() {
		return username;
	}

	public StringProperty getPassword() {
		return password;
	}

	public BooleanProperty getAdmin() {
		return admin;
	}
	
	public IntegerProperty getId() {
		return id;
	}
	
	public StringProperty getAdminString() {
		System.out.println(admin);
		if (admin.getValue()) {
			return new SimpleStringProperty("Administrador");
		}
		return new SimpleStringProperty("Vendedor");
	}
	
}



	