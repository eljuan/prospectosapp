package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import code.prospectosapp.view.*;
import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.collections.*;

public class LoginController {

    @FXML
    private TextField username;

    @FXML
    private TextField password;
    
    private ObservableList<User> userData = FXCollections.observableArrayList();
    
    private MainApp mainApp;

	@FXML
	private void handleButtonAction(ActionEvent event) throws Exception {
		try {
			User user = User.doLogin(username.getText(),password.getText());
			if (user != null && user.getAdmin().getValue()) {
				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(MainApp.class.getResource("view/AdminMenu.fxml"));
					AnchorPane adminMenu = (AnchorPane) loader.load();
					MainApp.rootLayout.setCenter(adminMenu);
					//rootLayout.getChildren().add(login);
					AdminMenuController controller = loader.getController();
					controller.setMainApp(this.mainApp);

				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (user != null && !user.getAdmin().getValue()) {
				try {			
					MainApp.loggedUserSeller = user.getId().get();
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(MainApp.class.getResource("view/LeadsSeller.fxml"));
					AnchorPane adminMenu = (AnchorPane) loader.load();
					MainApp.rootLayout.setCenter(adminMenu);
					//rootLayout.getChildren().add(login);
					LeadsSellerController controller = loader.getController();
					controller.setMainApp(this.mainApp);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			if (e != null && e.getMessage().equals("invalid username / password")) {
				Alert a = new Alert(AlertType.INFORMATION);
		        a.setTitle("ProspectosApp");
		        a.setHeaderText("Error");
		        a.setContentText("Usuario o Clave Incorrecto");
				a.showAndWait();
			}
			throw e;
		}
	}
	
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    public ObservableList<User> getUsers() {
        return userData;
    }
    
}
