package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import code.prospectosapp.ApiAccess;

public class AddLeadDialogController {

	public class KeyValuePair {
		   private final String key;
		   private final String value;
		   public KeyValuePair(String key, String value) {
		   this.key = key;
		   this.value = value;
		   }
		  public String getKey()   {    return key;    }
		  public String toString() {    return value;  }
		}
	
    @FXML
    private TextField nameField;
    
    @FXML
    private TextField companyField;
    
    @FXML
    private TextField phoneField;
    
    @FXML
    private TextField emailField;
    
    @FXML
    private TextField addressField;
    
    @FXML
    private ChoiceBox<KeyValuePair> usersSelect = new ChoiceBox<KeyValuePair>();


    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    private MainApp mainApp;
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
   	
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     * 
     * @param person
     */

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * @throws Exception 
     */
    @FXML
    private void handleOk() throws Exception {
        if (isInputValid()) {
        	Integer a_user = Integer.valueOf(usersSelect.getValue().getKey());
        	if (a_user == null) {
        		a_user = 0;
        	}
            Boolean result = ApiAccess.addLead(nameField.getText(), companyField.getText(), phoneField.getText(), emailField.getText(), addressField.getText(), a_user);
            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "Nombre No Valido!\n"; 
        }
        if (companyField.getText() == null || companyField.getText().length() < 1) {
            errorMessage += "Empresa No Valida!\n"; 
        }
        if (phoneField.getText() == null || phoneField.getText().length() < 4) {
            errorMessage += "Telefono No Valido!\n"; 
        }
        if (emailField.getText() == null || emailField.getText().length() < 6) {
            errorMessage += "Email No Valido!\n"; 
        }
        if (addressField.getText() == null || addressField.getText().length() < 3) {
            errorMessage += "Direccion No Valida!\n"; 
        }
      
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
			Alert a = new Alert(AlertType.INFORMATION);
	        a.setTitle("ProspectosApp");
	        a.setHeaderText("Error");
	        a.setContentText(errorMessage);
			a.showAndWait();
            return false;
        }
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    	ObservableList<User> users = this.mainApp.getUsers();
    	users.forEach((user) -> { 
    	    StringProperty username = user.getUsername();
    	    Integer id_user = user.getId().get();
    	    if (user.getAdmin().get()){
    	    	return;
    	    }
    	    usersSelect.getItems().add(new KeyValuePair(String.valueOf(id_user), username.getValue()));
    	});
    } 
    
}
