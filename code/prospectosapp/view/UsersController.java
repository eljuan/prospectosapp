package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.MainApp;
import code.prospectosapp.model.Lead;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import java.sql.ResultSet;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;

public class UsersController {

    static ObservableList<User> userData = FXCollections.observableArrayList();

    
    private MainApp mainApp;

    @FXML
    private TableView<User> usersTable;
    @FXML
    private TableColumn<User, String> usernameColumn;
    @FXML
    private TableColumn<User, String> adminColumn;
    
    @FXML
    private void initialize() {
		userData.clear();
		try {
			User[] usersSet = ApiAccess.loadUsers();
			int i = 0;
			while(i < usersSet.length) {
				userData.add(usersSet[i]);
				i++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	public void handleAddUserButtonAction(ActionEvent event) throws Exception {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/AddUserDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        AddUserDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleChangePasswordButtonAction(ActionEvent event) throws Exception {
		StringProperty selectedUsername ;
		try {
	    	try {
		        selectedUsername = usersTable.getSelectionModel().getSelectedItem().getUsername();
	    	} catch(NullPointerException e) {
	    		return;
	    	}
	    	// IF NOT USER SELECTED DONT DO ANYTHING.
	    	MainApp.selectedUserAdmin = selectedUsername.getValue();
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/ChangePassword.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        ChangePasswordDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleRemoveUserButtonAction(ActionEvent event) throws Exception {
		StringProperty selectedUsername ;
		try {
	    	try {
		        selectedUsername = usersTable.getSelectionModel().getSelectedItem().getUsername();
	    	} catch(NullPointerException e) {
	    		return;
	    	}
	    	MainApp.selectedUserAdmin = selectedUsername.getValue();
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/RemoveUser.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	
	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(this.mainApp.getPrimaryStage());
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	
	        // Set the person into the controller.
	        RemoveUserDialogController controller = loader.getController();
			controller.setMainApp(this.mainApp);
	        controller.setDialogStage(dialogStage);
	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        //this.userData = FXCollections.observableArrayList();
	        this.initialize();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public void handleMainMenuButtonAction(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AdminMenu.fxml"));
			AnchorPane usersWindow = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			MainApp.rootLayout.setCenter(usersWindow);
			//rootLayout.getChildren().add(login);
			AdminMenuController controller = loader.getController();
			controller.setMainApp(this.mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
		usersTable.setItems(this.getUsers());
        usernameColumn.setCellValueFactory(cellData -> cellData.getValue().getUsername());
        adminColumn.setCellValueFactory(cellData -> cellData.getValue().getAdminString());
    }    
    
    static public ObservableList<User> getUsers() {
        return userData;
    }
}
