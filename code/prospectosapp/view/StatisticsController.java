package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import code.prospectosapp.ApiAccess;
import code.prospectosapp.MainApp;
import code.prospectosapp.model.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.*;
import java.sql.ResultSet;
import java.util.ArrayList;

import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.Group;
import javafx.beans.binding.Bindings;

public class StatisticsController {

	public class KeyValuePair {
		   private final String key;
		   private int value;
		   public KeyValuePair(String key, int value) {
		   this.key = key;
		   this.value = value;
		   }

		  public String getKey()   {    return key;    }

		  public int getValue() {    return value;  }
		  
		  public void setValue(int newValue) { 
			  value = newValue;
		  }
		}
	
	private int status1 = 0;
	private int status2 = 0;
	private int status3 = 0;
	private int status4 = 0;
	
    private ObservableList<Lead> leadData = FXCollections.observableArrayList();
    private ObservableList<User> users;
    
    XYChart.Series series1 = new XYChart.Series();
    XYChart.Series series2 = new XYChart.Series();
    XYChart.Series series3 = new XYChart.Series();
    XYChart.Series series4 = new XYChart.Series();
    
    private MainApp mainApp;
 

    private PieChart pieChart1 = new PieChart();
    private PieChart pieChart2 = new PieChart();
    final CategoryAxis xAxis = new CategoryAxis();
    final NumberAxis yAxis = new NumberAxis();
    final BarChart<String,Number> bc = new BarChart<String,Number>(xAxis,yAxis);
 
    
    @FXML
    private void initialize() {
    }
    
    int totalSuccess;
    int totalReject;
    int totalNew;
    int totalInProcess;
    
    @FXML
    private AnchorPane apChartId;
    
    @FXML
	public void handleButton1Action(ActionEvent event) throws Exception {
    	apChartId.getChildren().clear();
		apChartId.getChildren().add(pieChart1);
	}

    @FXML
	public void handleButton2Action(ActionEvent event) throws Exception {
    	apChartId.getChildren().clear();
		apChartId.getChildren().add(pieChart2);
	}
	
    @FXML
	public void handleButton3Action(ActionEvent event) throws Exception {
    	apChartId.getChildren().clear();
    	bc.getData().clear();
    	bc.getData().addAll(series1, series2, series3, series4); 
		apChartId.getChildren().add(bc);
	}
	
    @FXML
	public void handleButton4Action(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AdminMenu.fxml"));
			AnchorPane usersWindow = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			MainApp.rootLayout.setCenter(usersWindow);
			//rootLayout.getChildren().add(login);
			AdminMenuController controller = loader.getController();
			controller.setMainApp(this.mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    
    public void setMainApp(MainApp mainApp) {

        this.mainApp = mainApp;

		try {
			Lead[] leadsSet = ApiAccess.loadLeads();
			int i = 0;
			while(i < leadsSet.length) {
				leadData.add(leadsSet[i]);
				i++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		users = this.mainApp.getUsers();
		
        leadData.forEach((lead) -> { 
    	    switch(lead.getStatus().get()) {
    	    	case "0":
    	    		status1++;
    	    		break;
    	    	case "1":
    	    		status2++;
    	    		break;
    	    	case "2":
    	    		status3++;
    	    		break;
    	    	case "3":
    	    		status4++;
    	    		break;    	    		
    	    		
    	    }
    	});
        
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                new PieChart.Data("No Contactado", status1),
                new PieChart.Data("Contactado", status2),
                new PieChart.Data("No Interesado", status3),
                new PieChart.Data("Nuevo Cliente", status4));

        pieChart1.setMinHeight(500);
        pieChart1.setMinWidth(450);
        
        
        ArrayList<KeyValuePair> userLeadsList = new ArrayList<KeyValuePair>();        
        users.forEach((user) -> { 
        	if (user.getAdmin().get()) {
        		return;
        	}
    	    userLeadsList.add(new KeyValuePair(user.getUsername().get(), 0));
    	});          
        leadData.forEach((lead) -> { 
            users.forEach((user) -> { 
            	if (user.getId().get() == Integer.valueOf(lead.getAssigned().get())){
            		userLeadsList.forEach((user_) -> {
            			if (user_.getKey() == user.getUsername().get()) {
            				int newValue = user_.getValue() + 1;
            				user_.setValue(newValue);
            			}
            		});
        		};
        	});
    	});
        ObservableList<PieChart.Data> pieChart2Data = FXCollections.observableArrayList();
        userLeadsList.forEach((user_) -> {
			pieChart2Data.add(new PieChart.Data(user_.getKey(),(int)(user_.getValue())));
		});        
        pieChart2 = new PieChart(pieChart2Data);
        pieChart2.setMinHeight(500);
        pieChart2.setMinWidth(450);

        
        ObservableList<Lead> successLeadData = FXCollections.observableArrayList();
        ObservableList<Lead> rejectLeadData = FXCollections.observableArrayList();
        ObservableList<Lead> inProcessLeadData = FXCollections.observableArrayList();
        ObservableList<Lead> newLeadData = FXCollections.observableArrayList();
        
        leadData.forEach((lead) -> { 
    	    if(lead.getStatus().get().equals("3")) {
    	    	successLeadData.add(lead);
    	    }
    	    if(lead.getStatus().get().equals("2")) {
    	    	rejectLeadData.add(lead);
    	    }   
    	    if(lead.getStatus().get().equals("1")) {
    	    	inProcessLeadData.add(lead);
    	    }    
    	    if(lead.getStatus().get().equals("0")) {
    	    	newLeadData.add(lead);
    	    }     	    
    	});        
        
        
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String,Number> bc = 
            new BarChart<String,Number>(xAxis,yAxis);
        bc.setTitle("Desglose por Vendedores");
        xAxis.setLabel("Usuario");       
        yAxis.setLabel("Ventas Hechas");
        
        

        
        
        series1.setName("Nuevo Cliente");
        series2.setName("No Interesado");
        series3.setName("Contactado");
        series4.setName("No Contactado");        
        
        users.forEach((user) -> { ;
        	if (user.getAdmin().get()) {
        		return;
        	}
    	    userLeadsList.add(new KeyValuePair(user.getUsername().get(), 0));
    	    successLeadData.forEach((lead) -> { 
    	    	if (Integer.valueOf(lead.getAssigned().get()) == user.getId().get()) {
    	    		totalSuccess++;
    	    	}
    	    	});
    	    rejectLeadData.forEach((lead) -> { 
    	    	if (Integer.valueOf(lead.getAssigned().get()) == user.getId().get()) {
    	    		totalReject++;
    	    	}
    	    	});
    	    inProcessLeadData.forEach((lead) -> { 
    	    	if (Integer.valueOf(lead.getAssigned().get()) == user.getId().get()) {
    	    		totalInProcess++;
    	    	}
    	    	});
    	    newLeadData.forEach((lead) -> { 
    	    	if (Integer.valueOf(lead.getAssigned().get()) == user.getId().get()) {
    	    		totalNew++;
    	    	}
    	    	});    	    
    	    series1.getData().add(new XYChart.Data(user.getUsername().get(), totalSuccess));	
    	    series2.getData().add(new XYChart.Data(user.getUsername().get(), totalReject));
    	    series3.getData().add(new XYChart.Data(user.getUsername().get(), totalInProcess));
    	    series4.getData().add(new XYChart.Data(user.getUsername().get(), totalNew));
    	    
    	    totalSuccess = 0;
    	    totalReject = 0;
    	});
    	pieChartData.forEach(data ->
        data.nameProperty().bind(
                Bindings.concat(
                        data.getName(), " ", data.pieValueProperty()
                )
        )
    			);
    	
    	pieChart2Data.forEach(data ->
        data.nameProperty().bind(
                Bindings.concat(
                        data.getName(), " ", data.pieValueProperty()
                )
        )
    			);
    	
        pieChart1.setData(pieChartData);
        apChartId.getChildren().add(pieChart1);
    }
    
    
}
