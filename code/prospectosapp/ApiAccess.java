package code.prospectosapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.http.util.EntityUtils;
import org.json.*;

import code.prospectosapp.model.*;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.MultipartBody;

public class ApiAccess {

	static private String host = "localhost";
	static private String database = "prospectosapp";
	static private String user = "root";
	static private String passwd = "practica";
	static private String loggedUsername;
	static private String loggedPassword;	
	static private Boolean loggedAdmin;	

	static private String url = "http://bmxstore.co/listenPost.php";
	
	static public User loginUser(String username, String password) throws Exception {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "login")
				.field("username", username)
				.field("password", password)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			loggedUsername = username;
			loggedPassword = password;
			Boolean isAdmin = (jsonObject.get("admin").equals("1")) ? true : false;
			loggedAdmin = isAdmin;
			User userToReturn = new User(Integer.valueOf(jsonObject.get("id").toString()), username, password, isAdmin);
			return userToReturn;
		}
		return null;
	}

	static public User[] loadUsers() throws Exception {
		if (loggedAdmin) {
			try {
				HttpResponse<String> jsonResponse = Unirest.post(url)
						.field("action", "listUsers")
						.field("username", loggedUsername)
						.field("password", loggedPassword)
						.asString();
				System.out.println(jsonResponse.getBody());
				JSONArray jsonArray = new JSONArray(jsonResponse.getBody());
				User[] users = new User[jsonArray.length()];
				
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject userObject = jsonArray.getJSONObject(i);		
					Boolean isAdmin = (userObject.get("admin").equals("1")) ? true : false;
					User userToReturn = new User((Integer) userObject.getInt("id"),(String) userObject.get("username"),(String) userObject.get("password"), isAdmin);
					users[i] = userToReturn;
				}
				return users;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}	
		} else return null;
	}

	static public Boolean AddUser(String username, String password, String adminInt) throws Exception {
		try {
			HttpResponse<String> jsonResponse = Unirest.post(url)
					.field("action", "insertUser")
					.field("username", loggedUsername)
					.field("password", loggedPassword)
					.field("i_username", username)
					.field("i_password", password)
					.field("i_admin", adminInt)
					.asString();
			JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
			if (jsonObject.get("result").equals(true)) {
				return true;
			}
			return false;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

static public Boolean changePass(String username, String password) throws Exception {
	try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "changePassword")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("c_username", username)
				.field("c_password", password)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			return true;
		}
		return false;
	} catch (Exception e) {
		System.err.println(e.getMessage());
		return false;
	}
}


static public Boolean removeUser(String username) throws Exception {
	try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "removeUser")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("r_username", username)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			return true;
		}
		return false;
	} catch (Exception e) {
		System.err.println(e.getMessage());
		return false;
	}
}

static public Boolean removeLead(Integer lead) throws Exception {
	try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "removeLead")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("r_lead", lead)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			return true;
		}
		return false;
	} catch (Exception e) {
		System.err.println(e.getMessage());
		return false;
	}
}

static public Lead[] loadLeads() throws Exception {
	if (loggedAdmin) {
		try {
			HttpResponse<String> jsonResponse = Unirest.post(url)
					.field("action", "listLeads")
					.field("username", loggedUsername)
					.field("password", loggedPassword)
					.asString();
			JSONArray jsonArray = new JSONArray(jsonResponse.getBody());
			Lead[] leads = new Lead[jsonArray.length()];
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject userObject = jsonArray.getJSONObject(i);
				Lead leadToReturn = new Lead((Integer) userObject.getInt("id"),(String) userObject.get("name"),(String) userObject.get("company"),(String) userObject.get("phone"), (String) userObject.get("address"), (String) userObject.get("email"), (String) userObject.get("status"),(String) userObject.get("assigned_user"));
				leads[i] = leadToReturn;
			}
			return leads;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	} else return null;
}

static public Boolean addLead(String name, String company, String phone, String email, String address,
	Integer a_user) throws Exception {		
		try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "insertLead")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("l_name", name)
				.field("l_company", company)
				.field("l_email", email)
				.field("l_address", address)		
				.field("l_phone", phone)	
				.field("l_assigned_user", a_user)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			return true;
		}
		return false;
	} catch (Exception e) {
		System.err.println(e.getMessage());
		return false;
	}
}
	
static public Lead[] loadLeadsSeller(Integer seller) throws Exception {
	try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "listLeadsUser")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("u_user", seller)
				.asString();
		JSONArray jsonArray = new JSONArray(jsonResponse.getBody());
		Lead[] leads = new Lead[jsonArray.length()];
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject userObject = jsonArray.getJSONObject(i);
			Lead leadToReturn = new Lead((Integer) userObject.getInt("id"),(String) userObject.get("name"),(String) userObject.get("company"),(String) userObject.get("phone"), (String) userObject.get("address"), (String) userObject.get("email"), (String) userObject.get("status"),(String) userObject.get("assigned_user"));
			System.out.println(leadToReturn.getName());
			leads[i] = leadToReturn;
		}
		return leads;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}	
}


static public Boolean changeAssignedUser(Integer lead, Integer newUser) throws Exception {
	try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "assignedUser")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("r_lead", lead)
				.field("r_newuser", newUser)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			return true;
		}
		return false;
	} catch (Exception e) {
		System.err.println(e.getMessage());
		return false;
	}
}

static public Boolean changeLeadStatus(Integer lead, Integer newStatus, Integer user) throws Exception {
	try {
		HttpResponse<String> jsonResponse = Unirest.post(url)
				.field("action", "changeStatusLead")
				.field("username", loggedUsername)
				.field("password", loggedPassword)
				.field("u_lead", lead)
				.field("u_leadstatus", newStatus)
				.field("u_user", user)
				.asString();
		JSONObject jsonObject = new JSONObject(jsonResponse.getBody());
		if (jsonObject.get("result").equals(true)) {
			return true;
		}
		return false;
	} catch (Exception e) {
		System.err.println(e.getMessage());
		return false;
	}
}

}

