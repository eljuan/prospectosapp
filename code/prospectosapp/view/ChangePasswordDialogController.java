package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.collections.*;
import code.prospectosapp.ApiAccess;

public class ChangePasswordDialogController {

    @FXML
    private TextField newPasswordField;
    @FXML
    private TextField newPasswordField2;

    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    private MainApp mainApp;
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     * 
     * @param person
     */

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * @throws Exception 
     */
    @FXML
    private void handleOk() throws Exception {
        if (isInputValid()) {
            String newPassword  = newPasswordField.getText();
            String username = MainApp.selectedUserAdmin;
            Boolean result = ApiAccess.changePass(username, newPassword);
            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (newPasswordField.getText() == null || newPasswordField.getText().length() < 6) {
            errorMessage += "Introduzca la nueva clave\n"; 
        }
        if (newPasswordField2.getText() == null || newPasswordField2.getText().length() < 6) {
            errorMessage += "Introduzca de nuevo la nueva clave!\n"; 
        }

        if (!errorMessage.isEmpty()) {
            return false;
        }
        if (newPasswordField2.getText().equals(newPasswordField.getText())) {
            return true;
        } else {
            // Show the error message.
            return false;
        }
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    } 
    
}
