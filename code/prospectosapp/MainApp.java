package code.prospectosapp;

import java.awt.event.*;
import java.io.IOException;

import javafx.collections.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.*;
import javafx.util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import code.prospectosapp.model.*;
import code.prospectosapp.view.*;
import java.sql.*;

public class MainApp extends Application {

	private Stage primaryStage;
	static public BorderPane rootLayout;
	static public String selectedUserAdmin;
	static public Integer selectedLeadAdmin;
	static public Integer selectedLeadUser;
	static public Integer loggedUserSeller;
	private ObservableList<User> theUsers = FXCollections.observableArrayList();
	private ObservableList<Lead> theAdminLeads = FXCollections.observableArrayList();

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Prospectos App v0.1");
		initRootLayout();
		showLogin();
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.setHeight(600);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shows the person overview inside the root layout.
	 */
	public void showLogin() {
		try {
			// Load person overview.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
			AnchorPane login = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			rootLayout.setCenter(login);
			// rootLayout.getChildren().add(login);
			LoginController controller = loader.getController();
			controller.setMainApp(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public ObservableList<User> getUsers() {
		return theUsers;
	}

	public void setUsers(ObservableList<User> newUsers) {
		theUsers = newUsers;
	}

	public ObservableList<Lead> getAdminLeads() {
		return theAdminLeads;
	}

	public void setAdminLeads(ObservableList<Lead> newAdminLeads) {
		theAdminLeads = newAdminLeads;
	}

	public static void main(String[] args) {
		launch(args);
	}
}