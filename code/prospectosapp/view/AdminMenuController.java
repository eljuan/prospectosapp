package code.prospectosapp.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import code.prospectosapp.ApiAccess;
import code.prospectosapp.MainApp;
import code.prospectosapp.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.collections.*;

public class AdminMenuController {
    
    private MainApp mainApp;
    
	@FXML
	private void handleUsersButtonAction(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Users.fxml"));
			AnchorPane usersWindow = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			MainApp.rootLayout.setCenter(usersWindow);
			//rootLayout.getChildren().add(login);
			UsersController controller = loader.getController();
			controller.setMainApp(this.mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void handleLeadsButtonAction(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Leads.fxml"));
			AnchorPane usersWindow = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			MainApp.rootLayout.setCenter(usersWindow);
			//rootLayout.getChildren().add(login);
			LeadsController controller = loader.getController();
			controller.setMainApp(this.mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void handleStatisticsButtonAction(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Statistics.fxml"));
			AnchorPane usersWindow = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
			MainApp.rootLayout.setCenter(usersWindow);
			//rootLayout.getChildren().add(login);
			StatisticsController controller = loader.getController();
			controller.setMainApp(this.mainApp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        final ObservableList<User> theUsers = FXCollections.observableArrayList();

		try {
			User[] userSet = ApiAccess.loadUsers();		
			int i = 0;
			while(i < userSet.length) {;
			theUsers.add(userSet[i]);
				i++;
			}
			this.mainApp.setUsers(theUsers);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }    
}
